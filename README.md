# meshToShelf

Save meshes to Maya shelf

## Installation steps:

1. Download and copy [meshToShelf.py][1] file and move it to Maya's script folder
2. Open a python tab in Maya's script editor and type the following
   
   ```python
   import meshToShelf;meshToShelf.create_shelf(shelf_name="Primitives")  # you may change the shelf_name according to your requirement.
   ```

3. Running the above script will create a new tab and add SaveToShelf button in it. 

_Note: In step 2, if you are using a shelf that already exist, the script will try to create a save button in the shelf if it doesn't exist_

## Usage:

After the installation, select any mesh object and click the SaveToShelf button to save mesh objects to the shelf

[1]: https://gitlab.com/vaishak-p/meshtoshelf/-/blob/main/meshToShelf.py "link"
