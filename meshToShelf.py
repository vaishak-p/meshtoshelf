"""
Installation steps:
-------------------
1: Copy this python script to Maya's script folder
2: Open a python tab in Maya's script editor and type the following
   
   import meshToShelf;meshToShelf.create_shelf(shelf_name="Primitives")  # you may change the shelf_name according to your requirement.

3: Running the above script will create a new tab and add SaveToShelf button in it. 

Note: In step 2, if you are using a shelf that already exist, the script will try to create a save button in the shelf if it doesn't exist

Usage:
-------------------
After the installation, select any mesh object and click the SaveToShelf button to save mesh objects to the shelf
"""


from maya.api import OpenMaya as om2
from maya import cmds
from maya import mel
from PySide2 import QtWidgets
import json


def create_shelf(shelf_name="Primitives"):
    shelf = mel.eval("$tmpVar=$gShelfTopLevel")
    
    existing = cmds.tabLayout(shelf, q=1, ca=1)
    
    if shelf_name not in existing:
        cmds.shelfLayout(shelf_name, p=shelf)

    existing_buttons = [cmds.shelfButton(x, q=True, iol=True) for x in (cmds.shelfLayout(shelf_name, q=True, ca=True) or list())]
    button_name = "SaveToShelf"
    if button_name in existing_buttons:
        return
    cmds.shelfButton(imageOverlayLabel="SaveToShelf", p=shelf_name, st="textOnly", stp="python",
    c='import meshToShelf;meshToShelf.save_item_to_shelf(None, "{}")'.format(shelf_name), rpt=False, image1="saveToShelf.png")        
    mel.eval("setShelfStyle `optionVar -query shelfItemStyle` `optionVar -query shelfItemSize`;")   


def save_item_to_shelf(obj=None, shelf_name="Primitives"):
    if not obj:
        obj = (cmds.ls(sl=True, long=True) or [None])[0]
    if not obj:
        return
    if not cmds.objExists(obj):
        return

    shape = obj if cmds.objectType(obj) == "mesh" else (cmds.listRelatives(obj, s=True, typ="mesh") or [None])[0]
    if not shape:
        return
    
    text = QtWidgets.QInputDialog.getText(None, "Shelf button name", "What would you like to call the new shelf button?", QtWidgets.QLineEdit.Normal, "<prim>")
    if not text[0]:
        cmds.warning("No name, no button! booo!!")
        return
    sel = om2.MSelectionList()
    sel.add(shape)
    
    dep = sel.getDependNode(0)
    
    data = {"object_data": get_mesh_data(dep)}

    shelf = mel.eval("$tmpVar=$gShelfTopLevel")
    
    existing = cmds.tabLayout(shelf, q=1, ca=1)
    
    if shelf_name not in existing:
        cmds.shelfLayout(shelf_name, p=shelf)
    
    cmds.shelfButton(imageOverlayLabel=text[0], p=shelf_name, st="textOnly", stp="python",
    c='import meshToShelf;meshToShelf.create_final_mesh({})'.format(json.dumps(data)), rpt=False, image1="polyMesh.png")        
    mel.eval("setShelfStyle `optionVar -query shelfItemStyle` `optionVar -query shelfItemSize`;")    


def create_final_mesh(data):
    dag_modifier = om2.MDagModifier()
    transform = dag_modifier.createNode("transform")
    mesh = create_node_from_data(data, transform, dag_modifier)
    
    dag_modifier.doIt()
    dag = om2.MFnDependencyNode(transform)
    
    shapes = cmds.listRelatives(dag.name(), s=1, f=1)
    cmds.delete(shapes[-1])
    cmds.select(shapes[0])
    cmds.polySoftEdge(shapes[0], a=0, ch=0)
    cmds.hyperShade(a="lambert1")  
    cmds.select(dag.name())
    return transform  


def get_mesh_data(obj):
    if not isinstance(obj, om2.MObject):
        logger.error("Invalid object")
        return
    if not obj.hasFn(om2.MFn.kMesh):
        logger.error("Object is not a mesh")
        return
    mesh = om2.MFnMesh(obj)
    mesh_data = dict([
        ("num_polygons", mesh.numPolygons), ("num_vertices", mesh.numVertices), ("name", mesh.name()),
        ("type", mesh.typeName), ("type_id", int(mesh.typeId.id())), ("points", maya_datatype_to_py(mesh.getPoints())),
        ("polygon_vertices", [maya_datatype_to_py(mesh.getPolygonVertices(i)) for i in range(mesh.numPolygons)]),
        ("uv_sets", mesh.getUVSetNames()), ("uvs", maya_datatype_to_py(mesh.getUVs())),
        ("num_polygon_vertices", [mesh.polygonVertexCount(i) for i in range(mesh.numPolygons)])])

    return mesh_data


def create_mesh_from_data(node, parent, data):
    if not isinstance(node, om2.MObject):
        return
    if not isinstance(data, dict):
        return

    mesh = om2.MFnMesh(node)
    mesh_node = mesh.create([om2.MPoint(x) for x in data["points"]],
                            data["num_polygon_vertices"],
                            flatten_nested_list(data["polygon_vertices"]), parent=parent)
    return mesh_node


def flatten_nested_list(target):
    result = list()
    for each in target:
        result.extend(each)
    return result


def maya_datatype_to_py(point_array):
    if isinstance(point_array, (list, tuple)):
        return [maya_datatype_to_py(x) for x in point_array]
    if not isinstance(point_array, (om2.MPointArray, om2.MIntArray, om2.MFloatArray, om2.MVector, om2.MPoint,
                                    om2.MMatrix)):
        cmds.error("invalid input")
        return
    if isinstance(point_array, om2.MPointArray):
        return [list(x) for x in point_array]
    elif isinstance(point_array, (om2.MIntArray, om2.MFloatArray, om2.MVector, om2.MPoint, om2.MMatrix)):
        return list(point_array)  


def create_node_from_data(data, parent, dag_modifier, shared_data=None):
    if not isinstance(data, dict):
        cmds.error("Invalid data. Could not generate a node")
        return
    object_data = data.get("object_data")
    if not object_data:
        cmds.error("invalid/incomplete data")
        return
    object_type = object_data.get("type")

    if not isinstance(dag_modifier, om2.MDagModifier):
        dag_modifier = om2.MDagModifier()

    node = dag_modifier.createNode(om2.MTypeId(object_data["type_id"]), parent or om2.MObject.kNullObj)
    dag_fn_node = om2.MFnDagNode(node)
    dag_fn_node.setName(object_data["name"])

    if object_type == "mesh":
        create_mesh_from_data(node, parent, object_data)

    return node          
